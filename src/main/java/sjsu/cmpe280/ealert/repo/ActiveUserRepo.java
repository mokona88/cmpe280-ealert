package sjsu.cmpe280.ealert.repo;

import org.springframework.data.repository.CrudRepository;
import sjsu.cmpe280.ealert.dto.ActiveUser;

/**
 * Created by squall on 11/25/15.
 */
public interface ActiveUserRepo extends CrudRepository<ActiveUser, String>
{
    ActiveUser findOneByName(String name);
    void deleteOneByName(String name);
}