package sjsu.cmpe280.ealert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EAlertApplication {

    public static void main(String[] args) {
        SpringApplication.run(EAlertApplication.class, args);
    }
}
