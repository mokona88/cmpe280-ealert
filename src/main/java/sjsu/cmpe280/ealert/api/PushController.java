package sjsu.cmpe280.ealert.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sjsu.cmpe280.ealert.dto.ActiveUser;
import sjsu.cmpe280.ealert.repo.ActiveUserRepo;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by squall on 11/26/15.
 */
@RestController
@RequestMapping("/push")
public class PushController
{
    @Autowired
    private ActiveUserRepo auRepo;

    @Autowired
    private HttpServletResponse response;

    @RequestMapping(value="/{senderId}", method = RequestMethod.PUT)
    public void notifySystem(
            @PathVariable("senderId") String id,
            @RequestParam("lon") double lon,
            @RequestParam("lat") double lat,
            @RequestParam("msg") String msg
            )
    {
        response.setHeader("Access-Control-Allow-Origin", "*");
        ActiveUser user = auRepo.findOneByName(id);
        if (user == null)
        {
            user = new ActiveUser();
            user.setName(id);
        }
        user.setLon(lon);
        user.setLat(lat);
        user.setActive(true);
        user.setAlert(true);
        user.setMessage(msg);
        auRepo.save(user);
    }
}