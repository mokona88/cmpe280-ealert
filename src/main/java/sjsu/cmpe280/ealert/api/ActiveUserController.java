package sjsu.cmpe280.ealert.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sjsu.cmpe280.ealert.dto.ActiveUser;
import sjsu.cmpe280.ealert.repo.ActiveUserRepo;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by squall on 11/26/15.
 */
@RestController
@RequestMapping("/active")
public class ActiveUserController
{
    @Autowired
    ActiveUserRepo auRepo;

    @Autowired
    private HttpServletResponse response;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public Iterable<ActiveUser> getActiveUsers(
            @RequestParam("senderId") String id,
            @RequestParam("lon") double lon,
            @RequestParam("lat") double lat
    )
    {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(200);
        ActiveUser user = auRepo.findOneByName(id);
        if (user == null){
            user = new ActiveUser();
            user.setName(id);
        }
        user.setLon(lon);
        user.setLat(lat);
        user.setActive(true);
        user.setAlert(false);
        auRepo.save(user);
        return auRepo.findAll();
    }
}
