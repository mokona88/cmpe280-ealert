package sjsu.cmpe280.ealert.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sjsu.cmpe280.ealert.dto.ActiveUser;
import sjsu.cmpe280.ealert.repo.ActiveUserRepo;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by squall on 12/2/15.
 */
@RestController
@RequestMapping(value="/delete", method = RequestMethod.DELETE)
public class LogoutController
{
    @Autowired
    private ActiveUserRepo auRepo;

    @Autowired
    private HttpServletResponse response;

    @RequestMapping(value = "/{senderId}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable("senderId") String id)
    {
        response.setHeader("Access-Control-Allow-Origin", "*");
        auRepo.deleteOneByName(id);
    }
}
