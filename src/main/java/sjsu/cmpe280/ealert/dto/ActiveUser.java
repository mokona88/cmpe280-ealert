package sjsu.cmpe280.ealert.dto;

/**
 * Created by squall on 11/26/15.
 */
public class ActiveUser
{
    private String _id;
    private String name;
    private double lon = 0.0;
    private double lat = 0.0;
    private boolean active = false;
    private boolean alert = false;
    private String message;

    public String get_id()
    {
        return _id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getLat()
    {
        return lat;
    }

    public void setLat(double lat)
    {
        this.lat = lat;
    }

    public double getLon()
    {
        return lon;
    }

    public void setLon(double lon)
    {
        this.lon = lon;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isAlert()
    {
        return alert;
    }

    public void setAlert(boolean alert)
    {
        this.alert = alert;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}
