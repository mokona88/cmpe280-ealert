package sjsu.cmpe280.ealert.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import sjsu.cmpe280.ealert.dto.ActiveUser;
import sjsu.cmpe280.ealert.repo.ActiveUserRepo;

/**
 * Created by squall on 11/26/15.
 */
@Component
public class ScheduleTask
{
    @Autowired
    private ActiveUserRepo auRepo;

    // clean up activeUser per minute
    @Scheduled(fixedRate = 600000)
    public void cleanupActiveUser()
    {
        auRepo.deleteAll();
    }

    @Scheduled(fixedRate = 600000)
    public void initializeDB()
    {
    	String str = "Some Strings";
        ActiveUser user = new ActiveUser();
        user.setName("user1");
        user.setLat(37.3349632);
        user.setLon(-121.8832698);
        user.setAlert(false);
        user.setActive(true);
        user.setMessage(str);
        auRepo.save(user);

        user = new ActiveUser();
        user.setName("user2");
        user.setLat(37.338075);
        user.setLon(-121.8801863);
        user.setAlert(false);
        user.setActive(true);
        user.setMessage(str);
        auRepo.save(user);

        user = new ActiveUser();
        user.setName("user3");
        user.setLat(37.3374011);
        user.setLon(-121.8832279);
        user.setAlert(false);
        user.setActive(true);
        user.setMessage(str);
        auRepo.save(user);
    }
}
