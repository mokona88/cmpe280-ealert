//package sjsu.cmpe280.ealert.service;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import sjsu.cmpe280.ealert.dto.ActiveUser;
//import sjsu.cmpe280.ealert.repo.ActiveUserRepo;
//
//import javax.annotation.PostConstruct;
//
///**
// * Created by squall on 11/26/15.
// */
//@Service
//public class InitDBLoader
//{
//    private final ActiveUserRepo auRepo;
//
//    @Autowired
//    public InitDBLoader(ActiveUserRepo auRepo)
//    {
//        this.auRepo = auRepo;
//    }
//
//    @PostConstruct
//    public void initDB()
//    {
//        auRepo.deleteAll();
//        
//        String str = "Some Strings";
//
//        ActiveUser user = new ActiveUser();
//        user.setName("user1");
//        user.setLon(37.3349632);
//        user.setLat(-121.8832698);
//        user.setAlert(false);
//        user.setActive(true);
//        user.setMessage(str);
//        auRepo.save(user);
//
//        user = new ActiveUser();
//        user.setName("user2");
//        user.setLon(37.338075);
//        user.setLat(-121.8801863);
//        user.setAlert(false);
//        user.setActive(true);
//        user.setMessage(str);
//        auRepo.save(user);
//
//        user = new ActiveUser();
//        user.setName("user3");
//        user.setLon(37.3374011);
//        user.setLat(-121.8832279);
//        user.setAlert(false);
//        user.setActive(true);
//        user.setMessage(str);
//        auRepo.save(user);
//
//    }
//}
